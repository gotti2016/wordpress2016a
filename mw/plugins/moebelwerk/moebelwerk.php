<?php
/*
Plugin Name: Moebelwerk Widget
Author: Gotti
*/

add_action( 'widgets_init', 'mw_register_widgets' );
function mw_register_widgets() {
	register_widget( 'mw_widget' );
}


class mw_widget extends WP_Widget 
{	
	
	function __construct() {	
		$widget_ops = array(
				'classname'   => 'mw_widget_cls',
				'description' => 'Display subcategories of menu selected category' );
		parent::__construct( 'mw_widget', 'Moebelwerk Widget', $widget_ops );	
	}

	function form( $instance ) 
	{	

	}
	
	function update( $new_instance, $old_instance ) {

	}

	function widget($args, $instance)
	{
		if(get_queried_object()==null)
			return;
		
		$root= (get_queried_object()->parent==0) ?
			get_queried_object()->name : 
			get_cat_name(get_queried_object()->parent);
				
		echo("<div class='mw-categories'>");	
		echo("<ul>");
		
		foreach(get_categories() as $cat)
		{							
			if($cat->parent != null) 
			{
				$parent= get_cat_name($cat->parent);
			    if(strcmp($parent, $root)==0)
			    {
				    echo("<li><a href='".get_category_link($cat)."'/>");
				    echo($cat->name."</li>");
			    }
			}		
		};

		echo("</ul>");
		
	}
}

?>